import { browser, by, element } from 'protractor';

export class GitlabPagesAngular4DeployPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}
