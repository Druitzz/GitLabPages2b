import { GitlabPagesAngular4DeployPage } from './app.po';

describe('gitlab-pages-angular4-deploy App', () => {
  let page: GitlabPagesAngular4DeployPage;

  beforeEach(() => {
    page = new GitlabPagesAngular4DeployPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
